#include "heltec.h"  
#include "images.h"

#define BAND    915E6  

unsigned int counter = 0;
String rssi = "RSSI --";
String packSize = "--";
String packet ;

void logo()
{
  Heltec.display->clear();
  Heltec.display->drawXbm(0,5,logo_width,logo_height,logo_bits);
  Heltec.display->display();
}

void setup()
{    
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
 
  Heltec.display->init();
  Heltec.display->flipScreenVertically();  
  Heltec.display->setFont(ArialMT_Plain_10);
  logo();             
  delay(1500);
  Heltec.display->clear();
  
  Heltec.display->drawString(0, 0, "LoRa inicializada!");
  Heltec.display->display();
  delay(1000); 
}
void loop()
{ 
  int fenceValue=analogRead (13);
   
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_10);
  
  Heltec.display->drawString(0, 0, "Enviando pacotes: ");
  Heltec.display->drawString(90, 0, String(counter));  
  Heltec.display->display();

  // send packet
  LoRa.beginPacket();
  
  LoRa.setTxPower(14,RF_PACONFIG_PASELECT_PABOOST);
  if (fenceValue>2800){
    LoRa.println("Cerca Ativada");
    Heltec.display->drawString(0,20,String(fenceValue));
    Heltec.display->drawString(0,30,"Cerca Ativada");
    Heltec.display->display();
  }
  else{
    LoRa.println("Cerca Desativada");
    Heltec.display->drawString(0,20,String(fenceValue));
    Heltec.display->drawString(0,30,"Cerca Desativada");
    Heltec.display->display();
    delay(1000);
}  
  LoRa.println(String(fenceValue));
  LoRa.println(counter);
  LoRa.endPacket();

  counter++;
  digitalWrite(LED, HIGH);  
  delay(1000);                       
  digitalWrite(LED, LOW);    
  delay(1000);                      
}

