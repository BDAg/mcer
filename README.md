                                                         Monitoramento de Cerca Elétrica Rural

Monitoramento de cerca elétrica rural via conexão LoRa 868/915Mhz (WP-WAN). Posicionado ao lado do eletrificador da cerca rural, o sensor de
variação de corrente fará o monitoramento dos pulsos que percorrem a cerca. Os valores captados pelo sensor, serão processados pelo ESP32 LoRa
e enviados intactos para o EP32 LoRa GATEWAY, que em seguida enviará ao dados para o roteador local e em seguida para a nuvem do Microsoft
Azure.
Os dados serão processados e analisados pelo IOTHub Azure e separados pelos seguintes estados: “ Normal”- Não será enviado nenhum e-mail
para o administrador da cerca, “ Crítico”- Envia e-mail de alerta, avisando que houve algum problema com a cerca eletrificada para o
administrador.
                                                          
                                                                
[Matriz de  Habilidades/ Conhecimento e Cronograma](https://drive.google.com/file/d/1K4Ec5ZWcK2fu0Z3aw3xDmUk4XjV37O7W/view?usp=sharing)
                                                        
                                                        
                                                       Conheça nossa Equipe

        Diego Antunes 

![93222317_104598321220342_2556524530700910592_n](/uploads/17bb9d3a8766c01e8b41f455e755b7d5/93222317_104598321220342_2556524530700910592_n.ico)

        Carlos Nobuaki
![WhatsApp-Image-2020-09-25-at-21.11.46](/uploads/b207036ecb872bccab310eef7b45c9df/WhatsApp-Image-2020-09-25-at-21.11.46.ico)

        Guilherme
![WhatsApp-Image-2020-09-25-at-20.53.06](/uploads/bd549bceb5080ad8bd54eebe732d77ec/WhatsApp-Image-2020-09-25-at-20.53.06.ico)

        Fernando
![WhatsApp-Image-2020-09-25-at-20.56.57__1_](/uploads/a50423f80bb784bf0d1fa11e99a43ecf/WhatsApp-Image-2020-09-25-at-20.56.57__1_.ico)

        Vania Mirela
![WhatsApp-Image-2020-09-25-at-21.45.04](/uploads/f0cc3e0f8e3be5bd752bca3f9374362a/WhatsApp-Image-2020-09-25-at-21.45.04.ico)

